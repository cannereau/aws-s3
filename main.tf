# init S3 bucket
resource "aws_s3_bucket" "sb" {
  bucket = "${var.prefix}.${var.zone}"
  tags   = var.tags
}

# enforce ownership
resource "aws_s3_bucket_ownership_controls" "sb" {
  bucket = aws_s3_bucket.sb.id
  rule {
    object_ownership = "BucketOwnerEnforced"
  }
}

# enforce encryption
resource "aws_s3_bucket_server_side_encryption_configuration" "sb" {
  bucket = aws_s3_bucket.sb.id
  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

# block all public access
resource "aws_s3_bucket_public_access_block" "sb" {
  bucket                  = aws_s3_bucket.sb.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

# configure CORS
resource "aws_s3_bucket_cors_configuration" "sb" {
  count  = (length(var.cors_methods) > 0 ? 1 : 0)
  bucket = aws_s3_bucket.sb.id
  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = var.cors_methods
    allowed_origins = var.cors_origins
    expose_headers  = []
    max_age_seconds = 3600
  }
}
