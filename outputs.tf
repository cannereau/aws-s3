output "name" {
  value       = aws_s3_bucket.sb.id
  description = "Bucket's name"
}

output "arn" {
  value       = aws_s3_bucket.sb.arn
  description = "Bucket's ARN"
}

output "rdn" {
  value       = aws_s3_bucket.sb.bucket_regional_domain_name
  description = "Bucket's region-specific domain name"
}

output "prefix" {
  value       = var.prefix
  description = "Bucket's prefix"
}

output "zone" {
  value       = var.zone
  description = "DNS Zone"
}
