# AWS S3 Bucket

This is a Terraform module to build :
- AWS S3 Bucket

This module enforces *private access* and *encryption*  
The Bucket's name is built from parameters : **prefix.zone**

## Mandatory parameters :
- **prefix** : Bucket's prefix
- **zone** : DNS Zone (eg "aws.fotlan.com")
- **tags** : Application's tags

## Optional parameters to configure CORS :
- **cors_methods** : CORS allowed methods (eg ["GET", "POST"])
- **cors_origins** : CORS allowed origins (default value : "https://www.fotlan.com")

## Output values :
- **name** : Bucket's name
- **arn** : Bucket's ARN
- **rdn** : Bucket's region-specific domain name
- **prefix** : Bucket's prefix
- **zone** : DNS Zone (eg "aws.fotlan.com")
