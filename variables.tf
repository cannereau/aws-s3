variable "prefix" {
  type        = string
  description = "Bucket's prefix"
}

variable "zone" {
  type        = string
  description = "DNS Zone"
}

variable "tags" {
  type        = map(string)
  description = "Application's tags"
}

variable "cors_methods" {
  type        = list(string)
  default     = []
  description = "CORS allowed methods"
}

variable "cors_origins" {
  type        = list(string)
  default     = ["https://www.fotlan.com"]
  description = "CORS allowed origins"
}
